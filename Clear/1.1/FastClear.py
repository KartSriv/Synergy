#!/usr/bin/env python

# This is FastClear 1.1
# A utility in Synergy by @karthiksrivijay

# Modules
import os # For system commands

# For more information check: https://stackoverflow.com/questions/15867952/python-3-2-os-module-python-functions-or-linux-commands

os.system("clear") # Using os module and system function
